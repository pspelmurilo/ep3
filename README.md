# EP3 I-Need
##### Autores : Victor Yukio e Murilo Gomes
I-Need é uma aplicação web feita atravéés do framework ruby-on-rails como projeto da disciplina orientação a objetos da UnB-FGA. O I-Need é um site com o intuito de facilitar a vida das pessoas que precisam de algum serviço/produto e não sabem um lugar fácil de achar, ele permite que o usuário compra e venda produtos/serviços da maneira que ele quiser.

## Instruções de execução 

Ao clonar o repositório deve-se executar o comando `bundle install`, depois o comando `rake db:migrate` e por fim `rake db:seed` para que o servidor possa ser iniciado, para iniciar o servidor basta usar o comando do rails: `rails server` 

Algumas especificações:
- Versão do ruby: 2.5.5
- Versão do rails: 5.2.2
- Base de dados utilizada: PostgreSQL

## Instruções de uso

Para que o usuário tenha acesso aos serviços e produtos ele tem que estar logado em uma conta antes de tudo, ao estar logado o usuário tem as seguintes opções: ver todos os serviços disponíveis, ver as categorias disponíveis e logo, ver os serviços daquela categoria, ofertar um serviço, e ver os serviços que ele já ofertou. Ao mostrar um serviço, o usuáário tem as seguintes opções: Comprar o serviço, dar like ou dislike, isso caso o serviço nãão seja dele, se o serviço for dele são disponibilizadas as opções de deletar ou editar os campos do serviço/produto. O sistema dispõe de categorias pre-determinadas pelos desenvolvedores, e só podem ser modificadas através do console do rails e do arquivo `seeds.rb`.


