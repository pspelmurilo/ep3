class RemoveThingsFromServices < ActiveRecord::Migration[5.2]
  def change
    remove_column :services, :likes, :integer
    remove_column :services, :deslikes, :integer
  end
end
