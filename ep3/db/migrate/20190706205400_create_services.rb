class CreateServices < ActiveRecord::Migration[5.2]
  def change
    create_table :services do |t|
      t.string :titulo
      t.text :descricao
      t.references :user, foreign_key: true
      t.references :category, foreign_key: true
      t.integer :likes
      t.integer :deslikes
      t.float :preco
      t.string :horario

      t.timestamps
    end
  end
end
