# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
Category.create(titulo: 'Cursos')
Category.create(titulo: 'Software')
Category.create(titulo: 'Outros')
Category.create(titulo: 'Domiciliar')
Category.create(titulo: 'Troca e Venda de Produtos')
Category.create(titulo: 'Comidas')