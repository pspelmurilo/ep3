json.extract! service, :id, :titulo, :descricao, :user_id, :category_id, :likes, :deslikes, :preco, :horario, :created_at, :updated_at
json.url service_url(service, format: :json)
