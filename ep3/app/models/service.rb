class Service < ApplicationRecord
	
	paginates_per 10
  	belongs_to :user
  	belongs_to :category
  	acts_as_votable
end
