class ServicesController < ApplicationController
  before_action :set_service, only: [:show, :edit, :update, :destroy, :like, :dislike]
  before_action :authenticate_user!


  # GET /services
  # GET /services.json
  def index
    @services = Service.order(:titulo).page params[:page]
  end

  def my_services
  end

  # GET /services/1
  # GET /services/1.json
  def show
  end

  def comprar_servico
    flash[:notice] = "Compra realizada com sucesso!"
    redirect_to services_path
  end


  def like
    if !current_user.voted_up_on? @service
      @service.liked_by current_user
    elsif current_user.voted_up_on? @service
      @service.unliked_by current_user
    end
    if current_user.voted_down_on? @service
      @service.undisliked_by current_user        
    end
    redirect_to @service
  end

  def dislike
    if !current_user.voted_down_on? @service
      @service.downvote_from current_user
    elsif current_user.voted_down_on? @service
      @service.undisliked_by current_user
    end
    if current_user.voted_up_on? @service
      @service.unliked_by current_user 
    end
    redirect_to @service
  end
  # GET /services/new
  def new
    @service = Service.new
    @categories = Category.all
  end

  # GET /services/1/edit
  def edit
    @categories = Category.all
  end

  # POST /services
  # POST /services.json
  def create
    @service = Service.new(service_params)
    @categories = Category.all
    @service.category_id = params[:category_id]
    @service.user_id = current_user.id
    respond_to do |format|
      if @service.save
        format.html { redirect_to @service, notice: 'Service was successfully created.' }
        format.json { render :show, status: :created, location: @service }
      else
        format.html { render :new }
        format.json { render json: @service.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /services/1
  # PATCH/PUT /services/1.json
  def update
    @service.category_id = params[:category_id]
    respond_to do |format|
      if @service.update(service_params)
        format.html { redirect_to @service, notice: 'Serviço atualizado com sucesso.' }
        format.json { render :show, status: :ok, location: @service }
      else
        format.html { render :edit }
        format.json { render json: @service.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /services/1
  # DELETE /services/1.json
  def destroy
    @service.destroy
    respond_to do |format|
      format.html { redirect_to services_url, notice: 'Serviço deletado com sucesso.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_service
      @service = Service.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def service_params
      params.require(:service).permit(:titulo, :descricao, :user_id, :category_id, :preco, :horario)
    end
end
