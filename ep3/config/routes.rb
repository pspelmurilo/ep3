Rails.application.routes.draw do
  	resources :services do
  		member do
  			put 'like' => 'services#like'
  			put 'dislike' => 'services#dislike'

  		end
  		get '/services/:id/like' => 'services#like'

  	end
    get '/my' => 'services#my_services'  
    post '/services/compra' => 'services#comprar_servico'
  	resources :categories
  	devise_for :users
    post '/services/:id/edit' => 'services#edit'

    devise_scope :user do  
	     get '/users/sign_out' => 'devise/sessions#destroy'     
	  end
    root 'home#index'
end
